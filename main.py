import pygame
from pygame.constants import QUIT, K_DOWN, K_UP, K_RIGHT, K_LEFT
import random
import os
from PIL import Image, ImageOps

pygame.init()

FPS = pygame.time.Clock()

HEIGHT = 700
WIDTH = 1200
FONT = pygame.font.SysFont("Verdana", 20)

COLOR_WHITE = (255, 255, 255)
COLOR_GREEN = (0, 255, 0)
COLOR_BLACK = (0, 0, 0)
COLOR_BLUE = (0, 0, 255)
COLOR_RED = (255, 0, 0)

main_display = pygame.display.set_mode((WIDTH, HEIGHT))

bg = pygame.transform.scale(pygame.image.load("background.png"), (WIDTH, HEIGHT))
bg_X1 = 0
bg_X2 = bg.get_width()
bg_move = 3

IMAGE_PATH = "Goose"
PLAYER_IMAGES = os.listdir(IMAGE_PATH)
PLAYER_IMAGES.pop(0)

player_size = pygame.image.load("player.png").convert_alpha().get_size()
# (20, 20)
player = pygame.image.load("player.png").convert_alpha()
# pygame.Surface(player_size)
# player.fill(COLOR_BLACK)
player_rect = pygame.Rect(0, HEIGHT / 2 - player_size[1], *player_size)
# player.get_rect()
player_move_down = [0, 2]
player_move_up = [0, -2]
player_move_right = [2, 0]
player_move_left = [-2, 0]


def create_enemy():
    enemy_size = pygame.image.load("enemy.png").convert_alpha().get_size()
    # (30, 30)
    enemy = pygame.image.load("enemy.png").convert_alpha()
    # pygame.Surface(enemy_size)
    # enemy.fill(COLOR_RED)
    enemy_rect = pygame.Rect(
        WIDTH + enemy_size[0],
        random.randint(0 + enemy_size[1], HEIGHT - enemy_size[1]),
        *enemy_size
    )
    enemy_move = [random.randint(-6, -4), 0]
    return [enemy, enemy_rect, enemy_move]


def create_bonus():
    bon_size = pygame.image.load("bonus.png").convert_alpha().get_size()
    # (15, 15)
    bonus = pygame.image.load("bonus.png").convert_alpha()
    # pygame.Surface(bon_size)
    # bonus.fill(COLOR_BLUE)
    bon_rect = pygame.Rect(
        random.randint(0 + bon_size[0], WIDTH - bon_size[0]), 0 - bon_size[1], *bon_size
    )
    bon_move = [0, 1]
    return [bonus, bon_rect, bon_move]


CREATE_ENEMY = pygame.USEREVENT + 1
pygame.time.set_timer(CREATE_ENEMY, 1500)

CREATE_BONUS = pygame.USEREVENT + 2
pygame.time.set_timer(CREATE_BONUS, 3000)

CHANGE_IMAGE = pygame.USEREVENT + 3
pygame.time.set_timer(CHANGE_IMAGE, 200)

enemies = []
bonuses = []
score = 0

image_index = 0
cur_image = "player.png"

playing = True
restart = False
while playing:
    FPS.tick(120)
    for event in pygame.event.get():
        if event.type == QUIT:
            playing = False

        if event.type == CREATE_ENEMY:
            enemies.append(create_enemy())

        if event.type == CREATE_BONUS:
            bonuses.append(create_bonus())

        if event.type == CHANGE_IMAGE:
            player = pygame.image.load(
                os.path.join(IMAGE_PATH, PLAYER_IMAGES[image_index])
            )
            cur_image = os.path.join(IMAGE_PATH, PLAYER_IMAGES[image_index])
            image_index += 1
            if image_index >= len(PLAYER_IMAGES):
                image_index = 0

    if restart:
        score = 0
        enemies = []
        bonuses = []
        image_index = 0
        cur_image = "player.png"
        player = pygame.image.load("player.png").convert_alpha()
        player_rect = pygame.Rect(0, HEIGHT / 2 - player_size[1], *player_size)
        restart = False

    bg_X1 -= bg_move
    bg_X2 -= bg_move

    if bg_X1 < -bg.get_width():
        bg_X1 = bg.get_width()

    if bg_X2 < -bg.get_width():
        bg_X2 = bg.get_width()

    main_display.blit(bg, (bg_X1, 0))
    main_display.blit(bg, (bg_X2, 0))

    keys = pygame.key.get_pressed()

    if keys[K_DOWN] and player_rect.bottom < HEIGHT:
        player_rect = player_rect.move(player_move_down)

    if keys[K_UP] and player_rect.top > 0:
        player_rect = player_rect.move(player_move_up)

    if keys[K_RIGHT] and player_rect.right < WIDTH:
        if player_rect[0] < 0:
            im = Image.open(cur_image)
            ImageOps.mirror(im).save("mirrored.png")
            player = pygame.image.load("mirrored.png")
        player_rect = player_rect.move(player_move_right)

    if keys[K_LEFT] and player_rect.left > 0:
        if player_rect[0] > 0:
            im = Image.open(cur_image)
            ImageOps.mirror(im).save("mirrored.png")
            player = pygame.image.load("mirrored.png")
        player_rect = player_rect.move(player_move_left)

    for enemy in enemies:
        enemy[1] = enemy[1].move(enemy[2])

        if enemy[1].left < 0:
            enemies.pop(enemies.index(enemy))

        main_display.blit(enemy[0], enemy[1])

        if player_rect.colliderect(enemy[1]):
            restart = True

    for bonus in bonuses:
        bonus[1] = bonus[1].move(bonus[2])

        if bonus[1].bottom >= HEIGHT:
            bonuses.pop(bonuses.index(bonus))

        main_display.blit(bonus[0], bonus[1])

        if player_rect.colliderect(bonus[1]):
            score += 1
            bonuses.pop(bonuses.index(bonus))

    main_display.blit(player, player_rect)

    main_display.blit(FONT.render(str(score), True, COLOR_BLACK), (WIDTH - 50, 20))

    pygame.display.flip()
